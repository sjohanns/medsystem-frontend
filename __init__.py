
import os
import sys

from tests._config import Config

sys.path.insert(0,
                os.path.abspath(
                    os.path.join(os.path.dirname(__file__),
                                 './src/')
                ))

from application import app



if __name__ == '__main__':
    app.config.from_object(Config)
    app.run()
