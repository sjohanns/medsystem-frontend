
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length, InputRequired, EqualTo, Regexp, Email


# LOGIN and REGISTRATION

class RegistrationForm(FlaskForm):
    username = StringField('Username: ', [InputRequired(), Regexp('^[A-Z][a-z]*\s[A-Z][a-z]*', message="Ім'я було написано не вірно. ")])
    email = StringField('Email: ', [InputRequired(), Email('Не правильно введений Email')])
    password = PasswordField('Password: ', [InputRequired(), EqualTo('confirm',
                                                                   message='Password must match')])
    confirm = PasswordField('Confirm Password: ', [Length(min=6, max=232), InputRequired('Пароль потрібно заповнити')])
    work = SelectField('Your work: ', choices=[('expert', 'Expert'),
                                               ('patient', 'Patient'),
                                               ('admin', 'Admin')], validators=[InputRequired()])
    date = StringField('', [InputRequired()])
    submit = SubmitField('Registration')
class LoginForm(FlaskForm):
    email = StringField('Email: ', [InputRequired(), Email(message="Не правильно введений Email")])
    password = PasswordField('Password: ', [InputRequired('Пароль потрібно заповнити')])
    submit = SubmitField('Login')

# PATIENT

class InpForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired(), Length(min=2, max=10)])
    #desc =  StringField('Description', validators=[DataRequired(), Length(min=2)])
    diseases = SelectField('', choices=[('poliuria', 'Poliuria'),
                                        ('aretmia', 'Aretmia'),
                                        ('sifilis', 'Sifilis')],
                           validate_choice=False)
    ready = BooleanField(' ')
    submit = SubmitField('Send')

class PatientAdd(FlaskForm):
    name = StringField('Ім`я: ', validators=[DataRequired(), Length(min=2, max=40)])
    symptoms = SelectField('', choices=[('Поліурія', 'Поліурія'),
                                        ('Свербіж', 'Свербіж'),
                                        ('Кількість глюкози у плазмі крові', 'Кількість глюкози у плазмі крові')])
    values = StringField('Значення: ', validators=[DataRequired()])
    add = SubmitField('Додати')
