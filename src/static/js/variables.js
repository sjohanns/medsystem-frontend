
const DLIST = getData('diseases', '')
const SLIST = getData('symptoms', '')
const PLIST = getData('patients', '')

const MODAL = document.getElementById('new_old_modal')
const BACK = document.getElementById('background_modal')

var ON_MODALS = []

const symptomHTML = `
<div class='title'>
<input type='button' id='delete' class='little_button' value='&#x2715'></input>
<input type='button' id='hidden' class='little_button' value='⯈'></input>
<h2>Симптом:</h2>
<p class='gen_name' id='gen_name'></p>
</div>
<div class='body'>
<div class='symptom_name'>
<label for='name'>Ім'я</label>
<div class='block_inp_name'><input type='text' name='name' id='symptom_name' class='inp_name'></input></div>
</div>
<div class='type'>
<label for='types'>Тип</label>
<select id='types' class='types' name='types'>
<option value='bool'>Bool</option>
<option value='range'>Range</option>
<option value='diseases'>Disease</option>
</select></div>
<div class='value'>
<label for='value'>Значення</label>
<div id='block_value' class='block_value'>
<input type='text' class='inp_value' id='value' name='value' placeholder='400'>
</input></div></div>
<div class='cost'>
<label for='cost'>Коштовність</label>
<div id='block_cost' class='block_cost'>
<p class='gen_number'>0</p>
<input type='range' class='inp_cost' id='cost' name='cost' min='0' max='100' value='0'>
</input></div></div></div>
<input id='id_symptom' class='id_symptom hidden'>
`

const symptomOldHTML = `
<input type='button' id='delete' class='little_button' value='&#x2715'></input>
<div class='symptom_name'>
<div class='block_inp_name'><input type='text' name='name' id='symptom_name' class='inp_name' disabled></input></div>
</div>
<div class='type'>
<input id='types' class='types' name='types' disabled></input>
</div>
<div class='value'>
<div id='block_value' class='block_value'>
<input type='text' class='inp_value' id='value' name='value' placeholder='400'>
</input>
</div>
</div>
<input id='id_symptom' class='id_symptom hidden'>
`

const patientHTML = `
		<div class='title'>
			<input type="button" class="little_button hidden_button" id="symptoms_btn" name="symptoms_btn" value="⯈"><p class="name"></p>
		</div>
		<div class="symptoms hidden" id="symptoms"></div>
`

const patient_symptomHTML = `
<div class="name_symptom">
<label for="name">Ім'я: </label>
<input id="name" class="name_symptom_" name="name" value="Поліурія"></input>
</div>
<div class="value">
<label for="name">Значення:</label>
<input id="value" name="value" value="Наявна"></input>
</div>
`
