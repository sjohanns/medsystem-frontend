
document.addEventListener('keydown', (e) => {
		if (e.key === 'Escape') {
				for (let i = 0; i < ON_MODALS.length; i++) {
						if (!ON_MODALS[i].classList.contains('hidden')) {
								ON_MODALS[i].classList.add('hidden')
						}
				}
				ON_MODALS = []
		}
})

BACK.addEventListener('click', () => {
		if (!BACK.parentNode.classList.contains('hidden')) {
				BACK.parentNode.classList.add('hidden')
				ON_MODALS.remove(BACK.parentNode)
		}
})

const SEARCH_SYMPTOMS_LIST = document.getElementById('search_symptoms_list')

SLIST.then(data => {
		let _data = JSON.parse(data)
		for (let i = 0; i < _data.length; i++) {
				let name = _data[i]['name']
				const block = document.createElement('div')
				block.classList.add('search_symptom')
				block.name = name.toLowerCase()
				block.innerHTML = `<p>${name}</p>`
				block.addEventListener('click', () => {
						const object = _data[i]
						addNewSymptoms(object)
				})
				SEARCH_SYMPTOMS_LIST.append(block)
		}
}).catch(err => {console.log(err)})



function selectType (select, elem, name) {
		function _select() {
				let type = select.value
				if (type === 'bool') {
						input = document.createElement('input')
						input.type = 'checkbox'
						input.className = 'inp_value'
						input.id = 'value'
						input.name = 'value'
						elem.innerHTML = input.outerHTML
				} else if (type === 'range') {
						input = document.createElement('input')
						input.type = 'text'
						input.className = 'inp_value'
						input.id = 'value'
						input.name = 'value'
						input.placeholder = '> 1'
						input.addEventListener('keypress', function(event) {
								if (event.key === 'Enter') {
										let value = input.value.split(' ')
										console.log(value)
								}
						})
						elem.innerHTML = input.outerHTML
				} else if (type == 'diseases') {
						elem.childNodes = new Array()
						var choices = '<select id="old_diseases" class="old_diseases" name="old_diseases">'
						const diseases = DLIST.then(data => {
								var	data = JSON.parse(data)
								for (let i = 0; i < data.length; i++) {
										let option = `<option value='${data[i]["name"]}'>${data[i]['name']}</option>`
										choices += option
								}
								choices += '</select>'
								elem.innerHTML = choices
								elem.childNodes[0].onchange = () => {
										name.value = elem.childNodes[0].value
										name.onchange()
								}
						})
		    }
		}
		return _select
}
