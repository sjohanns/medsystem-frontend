
let patients_list = document.getElementById('search_patient_list')

PLIST.then(data => {
		let _data = JSON.parse(data)
		console.log(_data.length)
		for (let i = 0; i < _data.length; i++) {
				let cur = _data[i]
				let patient = document.createElement('div')
				patient.className = 'search_patient'
				patient.innerHTML  = `<p>${_data[i]['name']}</p>`
				patient.addEventListener('click', () => {
						let button = document.getElementById('patient')
						const name = _data[i]['name']
						button.value = name
				})
				patients_list.appendChild(patient)
		}
})
