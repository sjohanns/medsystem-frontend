
function hiddenCheck() {
		const check=document.getElementById('hidden');
		const body=document.getElementById('body');
		if (check.checked) {
				body.style.display = 'block';
		} else {
				body.style.display = 'none';
		}
}


function hidden(btn, block) {
		let CLICK = true
		function hiddenCheck() {
			if (CLICK) {
					CLICK = false
					block.style.display = "block"
					btn.value = "⯆"
					btn.classList.add('active')
			} else {
	        CLICK = true
					block.style.display = 'none'
					btn.value = "⯈"
					btn.classList.remove('active')
			}
		}
		return hiddenCheck
}


// TODO: rename
function addNewSymptoms(object=null, update=false) {
		const symptoms_list = document.getElementById('symptoms_list')
		symptom = document.createElement('div');
		symptom.classList.add('symptom');
		symptom.id='symptom';
		symptom.innerHTML = symptomHTML;
		let btn = (symptom.getElementsByClassName('title'))[0]
		let _hidden = btn.children[1]
		let _delete = btn.children[0]
		let block = (symptom.getElementsByClassName('body'))[0]
		let name = (symptom.getElementsByClassName('gen_name'))[0]
		let inp_name = (symptom.getElementsByClassName('inp_name'))[0]
		let _id = (symptom.getElementsByClassName('id_symptom'))[0]
		let types = (symptom.getElementsByClassName('types'))[0]
		let value = (symptom.getElementsByClassName('block_value'))[0]
		let cost  = (symptom.getElementsByClassName('inp_cost'))[0]
		let gen_number = (symptom.getElementsByClassName('gen_number'))[0]
		inp_name.oninput = () => {
				name.innerText = inp_name.value
		}
		inp_name.onchange = () => {
				name.innerText = inp_name.value
		}
		cost.addEventListener('input', () => {
				gen_number.innerText = cost.value / 100
		})
		if (object) {
				if (!update) {
						symptom.classList.add('old')
				}
				const typechange = selectType(types, value, inp_name)
				if (typeof object['value'] === 'boolean') {
						types.value = 'bool'
						inp_name.value = ''
						inp_name.onchange()
						typechange()
						value.childNodes[0].checked = object['value']
				} else if (typeof object['value'] === 'object') {
						types.value = 'range'
						inp_name.value = ''
						inp_name.onchange()
						typechange()
						value.childNodes[0].value = ''.concat(object['value'][0], ' ', object['value'][1])
				} else {
						types.value = 'diseases'
						inp_name.value = ''
						inp_name.onchange()
						value.childNodes[1].value = object['name']
						value.childNodes[1].disabled = true
				}
				inp_name.value = object['name']
				name.innerText = object['name']
				_id.value = object['_id']
		} else {
				symptom.classList.add('new')
				types.onchange = selectType(types, value, inp_name)
		}
		_hidden.addEventListener('click', hidden(_hidden, block))
		symptoms_list.append(symptom)
		const index = symptoms_list.childNodes.length - 1
		const _symptom = symptoms_list.childNodes[index]
		_delete.addEventListener('click', () => {
				_symptom.remove()
		})

}

function addOldSymptoms() {
		let modal = document.getElementById('search_modal_symptoms')
		modal.classList.remove('hidden')
		ON_MODALS.push(modal)
}

function searchDiseasesModal() {
		let modal = document.getElementById('search_modal_diseases')
		modal.classList.remove('hidden')
		ON_MODALS.push(modal)
}

function modalNewOld() {
		MODAL.classList.remove('hidden')
		ON_MODALS.push(MODAL)
}

document.addEventListener('keydown', (e) => {
		if (e.key === 'Escape') {
				for (let i = 0; i < ON_MODALS.length; i++) {
						if (!ON_MODALS[i].classList.contains('hidden')) {
								ON_MODALS[i].classList.add('hidden')
						}
				}
				ON_MODALS = []
		}
})


const SSEARCH = document.getElementById('search_symptoms')

var TIMER

SSEARCH.addEventListener('keyup', () => {
		clearTimeout(TIMER)

		TIMER = setTimeout(() => {
				const ALL_SEARCH_OBJECTS = document.getElementsByClassName('search_symptom')
				for (let i = 0; i < ALL_SEARCH_OBJECTS.length; i++) {
						if (ALL_SEARCH_OBJECTS[i].childNodes[0].innerText.toLowerCase().includes(SSEARCH.value.toLowerCase())) {
								ALL_SEARCH_OBJECTS[i].classList.remove('hidden')
						} else {
								ALL_SEARCH_OBJECTS[i].classList.add('hidden')
						}
				}
		}, 500)

})

try {
		const DSEARCH = document.getElementById('search_diseases')

		DSEARCH.addEventListener('keyup', () => {

				clearTimeout(TIMER)

				TIMER = setTimeout(() => {
						const ALL_SEARCH_OBJECTS = document.getElementsByClassName('search_disease')
						for (let i = 0; i < ALL_SEARCH_OBJECTS.length; i++) {
								if (ALL_SEARCH_OBJECTS[i].childNodes[0].innerText.toLowerCase().includes(DSEARCH.value.toLowerCase())) {
										ALL_SEARCH_OBJECTS[i].classList.remove('hidden')
								} else {
										ALL_SEARCH_OBJECTS[i].classList.add('hidden')
								}
						}
				}, 500)
		})
}
catch (err) {
		console.error(err)
}
