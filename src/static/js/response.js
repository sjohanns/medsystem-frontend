
function getData(it, args) {
		const response = new Promise((resolve, reject) => {
				const xhr = new XMLHttpRequest()
				xhr.open('GET', `/api.${it}${args}`)
				xhr.onload = () => resolve(xhr.responseText)
				xhr.onerror = () => reject(xhr.statusText)
				xhr.send()
		})

		return response.then(data => {
				return Promise.resolve(data)
		}).catch( err => {
				console.log(err)
				return -1
		})

}

function post(response, it) {
		return new Promise((resolve, reject) => {
				const xhr = new XMLHttpRequest()
				xhr.open('POST', `/api.${it}`, true)
				xhr.setRequestHeader('Content-Type', 'application/json')
				xhr.onload = () => resolve(xhr.responseText)
				xhr.onerror = () => reject(xhr.statusText)
				xhr.send(JSON.stringify(response))
		})
}

function put(response, it) {
		return new Promise((resolve, reject) => {
				const xhr = new XMLHttpRequest()
				xhr.open('PUT', `/api.${it}`, true)
				xhr.setRequestHeader('Content-Type', 'application/json')
				xhr.onload = () => resolve(xhr.responseText)
				xhr.onerror = () => reject(xhr.statusText)
				xhr.send(JSON.stringify(response))
		}).then(data => {
				console.log(data)
		})
}

function response_diseases() {
		let _diseases = []
		let disease_name = document.getElementById('disease_name')
		let description = document.getElementById('description')
		let symptoms = document.getElementById('symptoms_list')
		let _id = document.getElementById('id_disease')
		var disease = {
				'_id': _id.value,
				'name': disease_name.value,
				'description': description.value,
				'symptoms': []
		}
		let _symptoms = []
		for (let i = 0; i < symptoms.children.length; i++) {
				let cur = symptoms.children[i].children[1]
				let __id = symptoms.children[i].children[2].value
				var name = cur.children[0].children[1].children[0].value
				var name = cur.children[0].children[1].children[0].value
				var type = cur.children[1].children[1].value
				var value = cur.children[2].children[1].children[0]
				var cost = cur.children[3].children[1].children[1].value
				if (type === 'bool') {
						value = value.checked
				} else if (type === 'range') {
						value = value.value.split(' ')
						value = [value[0], Number(value[1])]
						if (value.length < 2) {
								value = [].concat('=', value)
						}
				} else {
						value = value.value
				}
				let symptom = {
						'_id': __id,
						'name': name,
						'value': value,
						'cost': cost / 100
				}
				disease['symptoms'].push(__id)
				_symptoms.push(symptom)
				if (!symptoms.children[i].classList.contains('old')) {
				}
		}
		_diseases.push(disease)
		const response = {
				'diseases': _diseases,
				'symptoms': _symptoms
		}
		console.log(_symptoms)
		var data = put(response, 'diseases')
}


function response_patient() {
		let patient_name = document.getElementById('patient_name')
		let symptoms = document.getElementById('list_symptoms')
		let _symptoms = []
		for (let i = 0; i < symptoms.children.length; i++) {
				let cur = symptoms.children[i]
				let name = cur.children[1].children[0].children[0].value
				let type = cur.children[2].children[0].value
				let __id = cur.children[4].value
				var value = cur.children[3].children[0].children[0]
				if (type === 'Bool') {
						value = value.checked
				} else if (type === 'Range') {
						value = value.value.split(' ')
						if (value.length < 2) {
								value = [].concat('=', value)
						}
						value = [value[0], Number(value[1])]
				} else {
						value = value.value
				}
				let symptom = {
						'_id': __id,
						'name': name,
						'value': value,
				}
				_symptoms.push(symptom)
		}
		patient = {
				"name": patient_name.value,
				"symptoms": _symptoms
		}
		var data = post(patient, 'patients')
}

function response_diagnostics() {
		let patient = document.getElementById('patient').value
		let level = document.getElementById('level').value
		let DIAGNOSTICS = post({'name': patient, 'level': level}, 'diagnostics')
		DIAGNOSTICS.then(data => {
				const _data = JSON.parse(data)
				console.log(_data)
				let block = document.getElementById('results')
				block.innerHTML = ''
				for (let i = 0; i < _data.length; i++) {
						div = document.createElement('div')
						div.className = 'result'
            div.innerHTML = `<label>${_data[i]['name']}</label><p>${ (_data[i]['cost'] * 100).toFixed(2) }%</p>`
						block.appendChild(div)
				}
		})
}
