
import json
import hashlib

isit = ['наявність', 'наявна', 'наявно', 'наявний']
isnt = ['відсутність', 'відстутня', 'відстутньо', 'відсутній']

def formatingData(data):
    symptoms = []
    for symptom, value  in zip(data['symptoms'], data['values']):
        if value in isit:
            symptoms.append({"name": symptom, "value": True})
        elif value in isnt:
            symptoms.append({"name": symptom, "value": False})
        else:
            symptoms.append({"name": symptom, "value": [value[2], value[0]] if len(value) > 2 else value })

    del data['add']
    del data['values']
    del data['symptoms']
    data['name'] = data['name'][0]
    data['csrf_token'] = data['csrf_token'][0]
    data['symptoms'] = symptoms
    return data

def loadDBFile(db):
    data = None
    try:
        with open(db, 'r') as f:
            data = json.load(f)
    except OSError as e:
        print("Problem with file data base: %s" % (e));
    return data

def dump(data, db='./data/db.json'):
    oldData = loadDBFile(db)
    key = hashlib.sha256(data['name'].encode())
    oldData[key.hexdigest()] = data
    with open(db, 'w', encoding='utf8') as f:
        json.dump(oldData, f, indent=2, ensure_ascii=False)

def delete(keys, db='./data/db.json'):
    data = loadDBFile(db)
    for key in keys:
        if data.get(key):
            del data[key]
        else:
            print('Not exist this key %s' % (key,))
    with open(db, 'w', encoding='utf8') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)

def restoreFile(db):
    data = {}
    dataFile = loadDBFile(db)
    for item in dataFile:
        key = hashlib.sha256(item['name'].encode())
        data[key.hexdigest()] = item
    print(data)
    with open(db, 'w', encoding='utf8') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)

#restoreFile('../data/polyuria2.json')

#dump(data)
