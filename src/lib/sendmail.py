
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os

subject = "Email"
body = "Hello World"
EMAIL = os.getenv('EMAIL_USER')
#recipients = ["tovstolaki62@gmail.com"]
PASS = os.getenv("EMAIL_PASS")

def send(subject, body, sender=EMAIL, recipients=EMAIL, password=PASS):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['from'] = sender
    if not isinstance(recipients, list):
        msg['to'] = recipients
    else:
        msg['to'] = ', '.join(recipients)
    msg.attach(MIMEText(body, 'html'))
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
#        smtp.ehlo()
#        smtp.starttls()
#        smtp.ehlo()

        smtp.login(sender, password)
        smtp.sendmail(sender, sender, msg.as_string())
    print('Ready')

#send(subject, body, EMAIL, recipients, PASS)
