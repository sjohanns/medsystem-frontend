
function addPatient() {
		let modal = document.getElementById('search_modal_patient')
		modal.classList.remove('hidden')
		ON_MODALS.push(modal)
}

const PSEARCH = document.getElementById('search_patients')

var TIMER

PSEARCH.addEventListener('keyup', () => {
		clearTimeout(TIMER)

		TIMER = setTimeout(() => {
				const ALL_SEARCH_OBJECTS = document.getElementsByClassName('search_patient')
				for (let i = 0; i < ALL_SEARCH_OBJECTS.length; i++) {
						if (ALL_SEARCH_OBJECTS[i].childNodes[0].innerText.toLowerCase().includes(PSEARCH.value.toLowerCase())) {
								ALL_SEARCH_OBJECTS[i].classList.remove('hidden')
						} else {
								ALL_SEARCH_OBJECTS[i].classList.add('hidden')
						}
				}
		}, 500)

})
