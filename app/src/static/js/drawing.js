var ON_MODALS = []

document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        for (let i = 0; i < ON_MODALS.length; i++) {
						ON_MODALS[i].classList.add('hidden')
				}
    }
};

BACK.addEventListener('click', () => {
		if (!BACK.parentNode.classList.contains('hidden')) {
				BACK.parentNode.classList.add('hidden')
				ON_MODALS.remove(BACK.parentNode)
		}
})
