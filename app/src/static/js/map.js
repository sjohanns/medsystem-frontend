const SIZE = 0.2;

drag = simulation => {

    function dragstarted(event, d) {
	if (!event.aТоctive) simulation.alphaTarget(0.3).restart();
	d.fx = d.x;
	d.fy = d.y;
    }

    function dragged(event, d) {
	d.fx = event.x;
	d.fy = event.y;
    }

    function dragended(event, d) {
	if (!event.active) simulation.alphaTarget(0);
	d.fx = null;
	d.fy = null;
    }

    return d3.drag()
	.on("start", dragstarted)
	.on("drag", dragged)
	.on("end", dragended);
}

zoomed = ({transform}) => {
    svg.attr("transform", transform);
    marker.attr("transfom", transform);
}

const zoom = d3.zoom()
      .scaleExtent([1, 400])
      .on("zoom", zoomed);

const svg = d3.select("#container")
      .attr("width", 1000)
      .attr("height", 1000)
      .attr("viewBox", [-1000 / 2, -1000 / 2, 1000, 1000])
      .attr("style", "position: relative; max-width: 100%; height: auto;")
      .call(zoom)
      .append("g")
      .attr("width", 1000)
      .attr("height", 1000);



const marker = svg.append('marker')
      .attr('id', 'triangle')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 100*SIZE)
      .attr('markerWidth', 14.5*SIZE)
      .attr('markerHeight', 14.5*SIZE)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');


var div = d3.select(".container").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0)
    .style("position", "absolute")
    .style("z-index", 3);;

var funs = d3.select(".funs")
    .style("opacity", 0)

var delete_button = d3.select("#delete").on("click", e => {
    deletes({"_id": funs._groups[0][0].id}, "diseases");
})

const tree = d3.tree().size([1000, 1000]);

getData('tree', '').then((data) => {
    let	d = JSON.parse(data);
    for (let i = 0; i < d.length; ++i) {

	const seed = Math.random() * 300;
	const symX = Math.floor((Math.random()  % 90) * 100);
	const symY = Math.floor((Math.random() % 90) * 100);
	console.log(symX, symY)
	// +(symX * (i*seed))

	const root = d3.hierarchy(d[i]);
	tree(root);
	const links = root.links();
	const nodes = root.descendants();


	const simulation = d3.forceSimulation(nodes)
	      .force("link", d3.forceLink(links).id(d => d.id).distance(20).strength(1))
	      .force("center", d3.forceManyBody().strength(-270*SIZE).theta(2))
	      .force("x", d3.forceX(seed + i * 50))
	      .force("y", d3.forceY(seed - i * 50));

	const link = svg.append("g")
	      .attr("stroke", "#747474")
	      .attr("stroke-opacity", 0.9)
	      .selectAll("line")
	      .data(links)
	      .join("line")
	      .attr('d', 'M0,0L0,0')
	      .attr("marker-end", "url(#triangle)");

	// Append nodes.
	const node = svg.append("g")
	      .attr("fill", "#fff")
	      .attr("stroke", "#000")
	      .attr("stroke-width", 0)
	      .selectAll("rect")
	      .data(nodes)
	      .join("rect")
	      .attr("rx", 0.8 + "px")
	      .attr("fill", d => {
		  if (d.data.type === "disease") {
		      return "#333333"
		  } else {
		      if (d.children) {
			  return "#760532"
		      } else {
			  return "#573"
		      }
		  }
	      })
	      .attr("stroke", d => d.children ? null : "#000")
	      .attr("width", 58*SIZE)
	      .attr("height", 25*SIZE)
	      .on("click", d => {
		  console.log(d);
		  funs.transition()
		      .attr("id", d.originalTarget.__data__.data._id)
		      .style("opacity", 1)
		      .style("left", d.layerX + "px")
		      .style("top", d.layerY + "px");
		  setTimeout(() => {
		      funs.style("opacity", 0);
		  }, 3000);

	      })
	      .call(drag(simulation));

	const text = svg.append("g")
	      .attr("class", "labels")
	      .selectAll("text")
	      .data(nodes)
	      .enter().append("text")
	      .style("font-size", (22 * SIZE) + "px")
	      .attr("fill", "#eeeeee")
	      .on("mouseover", (d) => {
		  console.log(d);
		  let num =  "<p>name: " + d.originalTarget.__data__.data.name + '</p>'
		      + (d.originalTarget.__data__.data.value ? "value: " + d.originalTarget.__data__.data.value.toString() : '' ) + '</p>'
		      + (d.originalTarget.__data__.data.cost ? "cost: " + d.originalTarget.__data__.data.cost.toString() : '' ) + '</p>'
		      + (d.originalTarget.__data__.data.description ? "description: " + d.originalTarget.__data__.data.description : '' ) + '</p>'
		      + "<p>id: " + d.originalTarget.__data__.data._id.toString() + "</p>";
		  div.transition()
		      .style("opacity", 1);
		  div.html(num)
		      .style("left", d.x + "px")
		      .style("top", d.y + "px");

	      })
	      .on('mouseout', (d) => {
		  div.transition()
		      .duration('200')
		      .style("opacity", 0);
	      })
	      .text(d => d.data.name.slice(0, 3));

	simulation.on("tick", () => {
	    link
		.attr("x1", d => d.source.x)
		.attr("y1", d => d.source.y)
		.attr("x2", d => d.target.x)
		.attr("y2", d => d.target.y);
	    node
		.attr("x", d => d.x-(25*SIZE))
		.attr("y", d => d.y-(9*SIZE));
	    text
		.attr("dx", d => d.x-4)
		.attr("dy", d => d.y+2);
	});
    }
});
