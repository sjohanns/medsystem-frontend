

function addNewSymptom() {
		let modal = document.getElementById('search_modal_symptoms')
		modal.classList.remove('hidden')
		ON_MODALS.push(modal)
}

function addNewSymptoms(object = null) {
		console.log(object)
		symptom = document.createElement('div')
		symptom.classList.add('symptom')
		symptom.id='symptom'
		symptom.innerHTML = symptomOldHTML
		let inp_name = (symptom.getElementsByClassName('inp_name'))[0]
		let _delete = (symptom.getElementsByClassName('little_button'))[0]
		let _id = (symptom.getElementsByClassName('id_symptom'))[0]
		let types = (symptom.getElementsByClassName('types'))[0]
		let value = (symptom.getElementsByClassName('block_value'))[0]
		const typechange = selectType(types, value, inp_name)
		if (typeof object['value'] === 'boolean') {
				types.value = 'Bool'
				value.childNodes[1].type = 'checkbox'
				value.childNodes[1].checked = object['value']
		} else if (typeof object['value'] === 'object') {
				types.value = 'Range'
				value.childNodes[1].type = 'text'
				value.childNodes[1].value = ''.concat(object['value'][0], ' ', object['value'][1])
		} else {
				types.value = 'Bool'
				value.childNodes[1].type = 'checkbox'
				value.childNodes[1].checked = false
		}
		inp_name.value = object['name']
		name.innerText = object['name']
		_id.value = object['_id']

		document.getElementById('list_symptoms').appendChild(symptom)
		const index = document.getElementById('list_symptoms').childNodes.length - 1
		const _symptom = document.getElementById('list_symptoms').childNodes[index]
		_delete.addEventListener('click', () => {
				_symptom.remove()
		})
}
