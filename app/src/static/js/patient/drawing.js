
let patients_list = document.getElementById('patients')

PLIST.then(data => {
		let _data = JSON.parse(data)
		console.log(_data.length)
		for (let i = 0; i < _data.length; i++) {
				let cur = _data[i]
				let patient = document.createElement('div')
				patient.className = 'patient'
				patient.innerHTML = patientHTML
				let name = patient.getElementsByClassName('name')[0]
				name.innerText  = _data[i]['name']
				let symptoms = patient.getElementsByClassName('symptoms')[0]
				let btn_hidden = patient.getElementsByClassName('hidden_button')[0]
				for (let j = 0; j < cur['symptoms'].length; j++) {
						let symptom = document.createElement('div')
						symptom.classList.add('symptom')
						symptom.innerHTML = patient_symptomHTML
						let name_symptom = symptom.getElementsByClassName('name_symptom')[0].children[1]
						let value_symptom = symptom.getElementsByClassName('value')[0].children[1]

						name_symptom.value = cur['symptoms'][j]['name']
						if (typeof cur['symptoms'][j]['value'] === 'boolean') {
								value_symptom.type = 'checkbox'
								value_symptom.checked = cur['symptoms'][j]['value']
						} else {
								value_symptom.value = cur['symptoms'][j]['value']
						}
						symptoms.appendChild(symptom)
				}
				btn_hidden.addEventListener('click', open(btn_hidden, symptoms))
				patients_list.appendChild(patient)
		}
})

function open(btn, block) {
		CLICK = false
		function openSymptoms() {
				if (CLICK != false) {
						CLICK = false
						block.classList.remove("hidden")
						btn.value = "⯆"
						btn.style.color = "white"
						btn.style.backgroundColor = "#747474"
				} else {
	         CLICK = true
					 block.classList.add("hidden")
					 btn.value = "⯈"
					 btn.style.color = "#747474"
			     btn.style.backgroundColor = "white"
			 }
	 }
	 return openSymptoms
}

const PSEARCH = document.getElementById('search_patient')

var TIMER

PSEARCH.addEventListener('keyup', () => {
		clearTimeout(TIMER)

		TIMER = setTimeout(() => {
				const ALL_SEARCH_OBJECTS = document.getElementsByClassName('patient')
				for (let i = 0; i < ALL_SEARCH_OBJECTS.length; i++) {
						if (ALL_SEARCH_OBJECTS[i].children[0].children[1].innerText.toLowerCase().includes(PSEARCH.value.toLowerCase())) {
								ALL_SEARCH_OBJECTS[i].classList.remove('hidden')
						} else {
								ALL_SEARCH_OBJECTS[i].classList.add('hidden')
						}
				}
		}, 500)

})
