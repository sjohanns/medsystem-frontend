

const SEARCH_DISEASES_LIST = document.getElementById('search_diseases_list')
const NAME = document.getElementById('disease_name')
const DESCRIPTION = document.getElementById('description')
const _ID = document.getElementById('id_disease')
console.log(_ID)

DLIST.then(data => {
		let _data = JSON.parse(data)
		for (let i = 0; i < _data.length; i++) {
				let name = _data[i]['name']
				const block = document.createElement('div')
				block.classList.add('search_disease')
				block.name = name.toLowerCase()
				block.innerHTML = `<p>${name}</p>`
				block.addEventListener('click', () => {
						const object = _data[i]
						getData('diseases', `?disease=${object['name']}`).then(data => {
								const redata = JSON.parse(data)
								const disease = JSON.parse(redata[0])
								var symptoms = redata[1]
								console.log(redata)
								NAME.type = 'input'
								NAME.value = disease['name']
								DESCRIPTION.value = disease['desc']
								_ID.value = disease['_id']

								document.getElementById('symptoms_list').innerHTML = ''
							  symptoms = JSON.parse(symptoms)
								for (let i = 0; i <symptoms.length; i++) {
										addNewSymptoms(symptoms[i], true)
								}
						})
						for (let i = 0; i < ON_MODALS.length; i++) {
								if (!ON_MODALS[i].classList.contains('hidden')) {
										ON_MODALS[i].classList.add('hidden')
								}
						}
						ON_MODALS = []
				})
				SEARCH_DISEASES_LIST.append(block)
		}
}).catch(err => {console.log(err)})
