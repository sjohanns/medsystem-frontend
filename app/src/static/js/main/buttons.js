

let menu = document.getElementById('menu')
let bar = document.getElementById('bar')

menu.value = false

menu.addEventListener('click', () => {
		if (menu.value) {
				bar.classList.add('hidden')
				menu.value = false
				menu.style.fill = "#e0e0e0"
		} else {
				bar.classList.remove('hidden')
				ON_MODALS.push(bar)
				menu.value = true
				menu.style.fill = "#ebebeb"
		}
})
