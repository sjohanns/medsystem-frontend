from flask import Flask, render_template, \
    url_for, flash, redirect, \
    request, make_response, session, \
    jsonify
from werkzeug.middleware.proxy_fix import ProxyFix
from werkzeug.security import generate_password_hash, check_password_hash

from extentions.forms import RegistrationForm, LoginForm

app = Flask(__name__)

import datetime
import json
import hashlib

app.permanent_session_lifetime = datetime.timedelta(days=10)


from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId
from bson import encode
from extentions.database import Medsystem
DB = Medsystem()

import requests
from requests.exceptions import ConnectionError

# TODO: tests files
'''
def sendmail(data):
    symptoms = ''
    for name, value in data['symptoms'].items():
        symptoms += '<li>%s : %s</li>' % (name, value)
    message = '
    <div class="msg" style="line-height: 0.7;">
    <h2> Patient %s </h2>
    <h3> Symptoms: </h3>
    <ul style="font-size: 15px; padding: 0;margin: 0 0 20px 0;">
    %s
    </ul>
    <p style="font-size: 10px;line-height: 1.1;">CSRF_TOKEN: <i>%s</i> </p>
    ' % (data['name'], symptoms, data['csrf_token'])
    send('New patient %s' % (data['name']), message)
'''

bar = {
    None: [('', '')],
    'expert': [('diagnostics', 'Дігностика'), ('diseases/new', 'Створення та змінна хвороби'), ('patients/new', 'Створення пацієнту'), ('patients/list', 'Пацієнти')],
    'patient': [('room', 'Особиста кімната'), ('diagnostics', 'Діагноз')],
    'admin': [('diagnostics', 'Дігностика'), ('diseases/new', 'Створення та змінна хвороби'), ('patients/new', 'Створення пацієнту'), ('patients/list', 'Пацієнти'), ('room', 'Особиста кімната'), ('diagnostics', 'Діагноз')]
}

class ObjectIdDecoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return json.JSONEncoder.default(self, obj)

# TODO: main body
@app.route('/')
@app.route('/home')
def home():
    if not session.get('logged'):
        return redirect(url_for('login'))
    else:
        if session.get('user'):
            name = session['user']['name']['firstname'] + '_' + session['user']['name']['lastname']
            return redirect('/%s/%s/room' % (name, session['user']['work']) )
        else:
            return render_template('index.html', title='Home', user=session.get('user', {'name': {'firstname': '', 'lastname': ''}}), bar=bar[session.get('user')['work']])

@app.route('/about')
def about():
    content = render_template('about', title='About')
    res = make_response(content)
    res.headers['Content-Type'] = 'text/plain'
    res.headers['Server'] = 'johann'
    return res

@app.route('/patient-valid', methods=['POST'])
def valid():
    req = request.form.lists()
    print([obj for obj in req])
    return redirect(url_for('patient'))

@app.route('/login', methods=['GET', 'POST'])
def login():
#    session.clear()
    form = LoginForm()
    if request.method == 'POST':
        user = DB['users'].find_one({'email': request.form['email']}, {})
        if user:
            if check_password_hash(user['password'], request.form['password']):
                user['_id'] = str(user['_id'])
                session['user'] = user
                session['logged'] = True
                flash('Ви успішно зарегіструвалися!', 'msg')
                name = user['name']['firstname'] + '_' + user['name']['lastname']
                return redirect('/%s/%s/room' % (name, user['work']))
            else:
                flash('Не вірний пароль', 'error')
        else:
            flash('Коритсувача на існує, спершу будь-лаcка зарегеструйтеся', 'error')
    return render_template('login.html', form=form, title='Login', user=session.get('user', {'name': {'firstname': '', 'lastname': ''}}))

@app.route('/logout')
def logout():
    session.pop('logged', None)
    session.pop('user', None)
    return redirect(url_for('home'))

@app.route('/registration', methods=['GET', 'POST'])
def registration():
    form = RegistrationForm()
    if not session.get('logged', False):
        session['logged'] = False
        session.permanent = True
        if request.method == 'POST':
            if request.form['password'] == request.form['confirm']:
                name = (request.form['username']).split()
                dtime = request.form['date']
                dtime = datetime.datetime.strptime(request.form['date'], '%m/%d/%Y')
                user = {
                    'name': {
                        'firstname': name[0],
                        'lastname': name[1]
                    },
                    'email': (request.form['email']).replace(' ', ''),
                    'password': generate_password_hash(request.form['password']),
                    'work': request.form['work'],
                    'date': dtime,

                }
                try:
                    error, response = DB.add_new_user(user)
                    if error:
                        session['logged'] = False
                        flash(error, 'error')
                    else:
                        session['logged'] = True
                        user['_id'] = str(response)
                        session['user'] = user
                        flash('Ви успішно зарегіструвалися!', 'msg')
                        name = user['name']['firstname'] + '_' + user['name']['lastname']
                        return redirect('/%s/%s/room' % (name, user['work']))
                except:
                    print('Something with inserting a new user')
            else:
                print('Warn')
                flash('Неправильно записан пароль', 'error')
    else:
        return redirect(url_for('home'))
    return render_template('registration.html', form=form, title='Registration', user=session.get('user', {'name': {'firstname': '', 'lastname': ''}}), bar=bar[session.get('user', {'work': None})['work']])


@app.route('/<name>/<status>/room')
def room(name, status):
    print('Hello World')
    return render_template('room.html', title='%s' % (name,), user=session.get('user'), bar=bar[session.get('user')['work']])

# Expert pages

@app.route('/<name>/<status>/patients/<method>')
def patients(name, status, method):
    if session['user']['work'] == status and status in ['admin', 'expert']:
        if method == 'list':
            return render_template('patients.html', title="Пацієнти", user=session.get('user'), bar=bar[session.get('user')['work']])
        elif method == 'new':
            return render_template('patient.html', title="Новий пацієнт", user=session.get('user'), bar=bar[session.get('user')['work']])
    else:
        return ('Вибачте, ви мабуть помилилися !!!', 404)


@app.route('/<name>/<status>/diseases/<method>')
def diseases(name='#', status='#', method='#'):
    if name == '#' or status == '#':
        name = session['user']['name']
        status = session['user']['work']
        return redirect('/%s/%s/diseases/%s' % (name, status, method))
    if session['user']['work'] == status and status in ['admin', 'expert']:
        return render_template(('%s_diseases.html' % (method,)), title="Вікно хвороб", user=session.get('user', {'name': {'firstname': '', 'lastname': ''}}), bar=bar[session.get('user')['work']])
    else:
        return ('Вибачте, ви мабуть помилилися !!!', 404)

@app.route('/<name>/<status>/diagnostics')
def diagnostics(name, status):
    print(session.get('user'))
    if session['user']['work'] == status and status in ['admin', 'expert']:
        try:
            status = requests.get('http://127.0.0.1:5050/api.test')
            return render_template('diagnostics.html', status='green', title="Діагностика", user=session.get('user'), bar=bar[session.get('user')['work']])
        except ConnectionError as e:
            return render_template('diagnostics.html', status='red', title="Діагностика", user=session.get('user'), bar=bar[session.get('user')['work']])
    else:
        return ('Вибачте, ви мабуть помилилися !!!', 404)


@app.route('/<name>/<status>/map')
def map(name, status):
    if session['user']['work'] == status and status in ['admin', 'expert']:
        return render_template('map.html', title="Дерево хвороб", user=session.get('user'), bar=bar[session.get('user')['work']])
    else:
        return ('Вибачте, ви мабуть помилилися !!!', 404)


@app.route('/api.diseases', methods=['POST', 'GET', 'PUT', 'DELETE'])
def api_deseases():
    diseases = None
    if request.method == 'POST':
        diseases = request.json['diseases']
        symptoms = request.json['symptoms']
        symptom_id = []
        try:
            for symptom in symptoms:
                result = DB.db['symptoms_test'].insert_one(symptom)
                symptom_id.append({'name': symptom['name'], '_id': result.inserted_id})
            for disease in diseases:
                symptoms = []
                cur = disease['symptoms']
                for symptom in cur:
                    for _id in symptom_id:
                        if symptom == _id['name']:
                            symptoms.append(_id['id'])
                disease['symptoms'] = symptoms
                result = DB.db.diseases_test.insert_one(disease)
        except DuplicateKeyError:
            flash('Хвороба із такою назвою вже існує.', 'error')
            return redirect('/%s/%s/new_diseases' % (session['user']['name'], session['user']['work']))
        print(request.json)
        return {'status': True}
    elif request.method == 'GET':
        if request.args.get('disease'):
            response = DB.db['diseases_test'].find_one({'name': request.args.get('disease')})
            symptoms = DB.db['symptoms_test'].find({'_id': {'$in': response['symptoms_id']}})
            return jsonify((json.dumps(response, cls=ObjectIdDecoder), json.dumps([obj for obj in symptoms], cls=ObjectIdDecoder)))
        else:
            diseases = DB.db['diseases'].find({}, {'_id': 0, 'name': 1})
            diseases = [disease for disease in diseases]
        return jsonify(diseases)
    elif request.method == 'PUT':
        diseases = request.json['diseases']
        symptoms = request.json['symptoms']
        _symptoms = []
        dsymptoms = []
        for symptom in symptoms:
            print(symptom)
            try:
                symptom['_id'] = hash(symptom['name'] + 'S')
                _id = DB.db['symptoms_test'].insert_one(symptom)
            except DuplicateKeyError as e:
                print("WARNING: Duplicate key")
            _symptoms.append(symptom['_id'])
        for disease in diseases:
            symptom = {'name': disease['name'], 'value': True, 'cost': 1, 'link': hash(disease['name'])}
            try:
                print("R3")
                disease['_id'] = hash(disease['name'])
                symptom['_id'] = hash(disease['name'] + "S")
                disease['symptoms'] = _symptoms
                if disease.get('_id', False):
                    DB.db['diseases_test'].delete_one({'_id': disease['_id']})
                else:
                    pass
                DB.db['diseases_test'].insert_one(disease)
            except DuplicateKeyError as e:
                pass
            dsymptoms.append(symptom)
        for dsymptom in dsymptoms:
            print("R1")
            try:
                DB.db['symptoms_test'].insert_one(dsymptom)
            except DuplicateKeyError as e:
                pass

        return {'status': 'ok'}
    elif request.method == 'DELETE':
        if (DB.db['symptoms_test'].delete_one({"_id": int(request.json["_id"])}).deleted_count):
            return {'status': 'ok'}
        else:
            for symptom in DB.db['diseases_test'].find_one({"_id": int(request.json["_id"])})['symptoms']:
                DB.db['symptoms_test'].delete_one({"_id": symptom})
            if (DB.db['diseases_test'].delete_one({"_id": int(request.json["_id"])}).deleted_count):
                return {'status': 'ok'}
            else:
                return {'status': 'problem'}

@app.route('/api.symptoms', methods=['POST', 'GET'])
def api_symptoms():
    symptoms = None
    if request.method == 'POST':
        return {'status': False}
    elif request.method == 'GET':

        symptoms = DB.db['symptoms_test'].find({})
        symptoms = [symptom for symptom in symptoms]
        symptoms = json.loads(json.dumps(symptoms, cls=ObjectIdDecoder))
    return jsonify(symptoms)

@app.route('/api.patients', methods=['POST', 'GET'])
def api_patient():
    if request.method == 'POST':
        patient = request.json
        if not patient['name']:
            flash("Немає призвища та ім'я", 'err')
            return {'status': False}
        if not patient['symptoms']:
            flash("Немає жодного симптому!", 'err')
            return {'status': False}
        DB.db['patient_test'].insert_one(patient)
        return {'status': True}
    elif request.method == 'GET':
        patients = DB.db['patient_test'].find({}, {'_id': 0})
        patients = [patient for patient in patients]
        return jsonify(patients)

@app.route('/api.diagnostics', methods=['POST'])
def api_diagnostics():
    if request.method == 'POST':
        data = request.json
        response = requests.post('http://127.0.0.1:5050/api.diagnostics', data=json.dumps(data)).json()
        print(response)
        if response['status']:
            return jsonify([response['result'], response['log']])
        else:
            flash(response['desc'], 'error')
            return {'status': False}

@app.route('/<name>/<status>/dynamic')
def dynamic(name, status):
    if session['user']['work'] == status and status in ['admin', 'patient']:
        return '<h2>Somethings</h2>'
    else:
        return ('Вибачте, ви мабуть помилилися !!!', 404)

@app.route('/api.tree', methods=['GET'])
def tree():
    if request.method == "GET":
        data = []
        for disease in DB['diseases_test'].find():
            _data = {}
            _data["name"] = disease["name"]
            _data["description"] = disease["description"]
            _data["_id"] = str(disease["_id"])
            _data["type"] = "disease"
            _data["children"] = []
            for symptom in DB["symptoms_test"].find({"_id": {"$in": disease["symptoms"]}}):
                if symptom.get("link", False):
                    print(symptom)
                    _disease = DB['diseases_test'].find_one({"_id": symptom['link']})
                    __data = {}
                    __data["name"] = _disease["name"]
                    __data["description"] = _disease["description"]
                    __data["_id"] = str(_disease["_id"])
                    __data["type"] = "disease"
                    __data["children"] = []
                    for _symptom in DB["symptoms_test"].find({"_id": {"$in": _disease["symptoms"]}}):
                        _symptom["_id"] = str(_symptom["_id"])
                        __data["children"].append(_symptom)
                    symptom["children"] = [__data]
                symptom["_id"] = str(symptom["_id"])
                _data["children"].append(symptom)
            data.append(_data)
        print(data)
        return jsonify(data)



@app.route('/test', methods=['GET', 'POST'])
def test():
    if request.method == 'POST':
        print(request.data)
    return redirect(url_for('home'))

@app.errorhandler(404)
def pageNot(error):
    return ('Вибачте, ви мабуть помилилися !!!', 404)
