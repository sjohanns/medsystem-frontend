
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length, InputRequired, EqualTo, Regexp, Email


# LOGIN and REGISTRATION

class RegistrationForm(FlaskForm):
    username = StringField("Ім'я: ", [InputRequired(), Regexp('^[A-Z][a-z]*\s[A-Z][a-z]*', message="Ім'я було написано не вірно. ")])
    email = StringField('Електронна почта: ', [InputRequired(), Email('Не правильно введений Email')])
    password = PasswordField('Пароль: ', [InputRequired(), EqualTo('confirm',
                                                                   message='Password must match')])
    confirm = PasswordField('Повторіть пароль: ', [Length(min=6, max=232), InputRequired('Пароль потрібно заповнити')])
    work = SelectField('Ваш статус: ', choices=[('expert', 'Expert'),
                                               ('patient', 'Patient'),
                                               ('admin', 'Admin')], validators=[InputRequired()])
    date = StringField('', [InputRequired()])
    submit = SubmitField('Регистрація')
class LoginForm(FlaskForm):
    email = StringField('Електронна почта: ', [InputRequired(), Email(message="Не правильно введений Email")])
    password = PasswordField('Пароль: ', [InputRequired('Пароль потрібно заповнити')])
    submit = SubmitField('Вхід')
