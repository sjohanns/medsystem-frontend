
import os
import urllib

from dotenv_vault import load_dotenv
load_dotenv()

from pymongo import MongoClient

NAME = urllib.parse.quote_plus(os.getenv('MONGODB_USERNAME'))
PASSWORD = urllib.parse.quote_plus(os.getenv('MONGODB_PASSWORD'))
DB = os.getenv('MONGODB_DATABASE')

class Medsystem:
    def __init__(self, id=''):
        self.id = id
        self.get_database()
    def __getitem__(self, items):
        return self.db[items]
    def get_database(self):
        self.client = MongoClient('localhost',
                    username=NAME,
                    password=PASSWORD,
                    authSource='medsystem')
        self.db = self.client[DB]

    def add_new_user(self, document):
        error = None
        response = False
        cursor = self.db.users.find_one({'email': document['email']}, {'_id': 1})
        if cursor:
            error = 'Користувач з таким email вже існує!'
        else:
            response = self.db.users.insert_one(document)
            response = response.inserted_id
        return error, response

if __name__ == '__main__':
    db = Medsystem()
    db.get_database()
    SERVER.stop()
