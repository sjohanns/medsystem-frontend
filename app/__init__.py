
import os
import sys

from tests._config import Config

sys.path.insert(0,
                os.path.abspath(
                    os.path.join(os.path.dirname(__file__),
                                 './src/')
                ))

from application import app



if __name__ == '__main__':
    app.config.from_object(Config)
    port = int(os.environ.get('APP_PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
